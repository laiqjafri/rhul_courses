//=============================================================================
//CS5234 -- Distributed WordCount MapReduce Program -- The Reducer
//=============================================================================

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WordCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

	// You can put instance variables here to store state between iterations of
	// the reduce task.
	private IntWritable result = new IntWritable();

	// The setup method. Anything in here will be run exactly once before the
	// beginning of the reduce task.
	public void setup(Context context) throws IOException, InterruptedException {
//		Properties props = new Properties();
//		props.put("log4j.rootLogger", "DEBUG,default.file");
//		props.put("log4j.appender.default.file", "org.apache.log4j.FileAppender");
//		props.put("log4j.appender.default.file.append", true);
//		props.put("log4j.appender.default.file.file", "/home/local/uxac007/mylogfile.log");
//		props.put("log4j.appender.default.file.layout", "org.apache.log4j.PatternLayout");
//		props.put("log4j.appender.default.file.layout.ConversionPattern", "%-5p %c: %m%n");
//		PropertyConfigurator.configure(props);
	}

	// The reducer method
	public void reduce(Text key, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {

		// You can emit output records using
		// context.write(new Text("some string"), new Text("some other string"));
		// Note: you can use Context methods in the setup and cleanup as well.

		int sum = 0;
		for (IntWritable val : values) {
			// accummulate the result in the 'sum' variable
			// sum += ...
		}
		
		// Set and out put the result
		result.set(sum);
		context.write(key, result);
	}

	// The cleanup method. Anything in here will be run exactly once after the
	// end of the reduce task.
	public void cleanup(Context context) throws IOException,
	InterruptedException {
	}

}