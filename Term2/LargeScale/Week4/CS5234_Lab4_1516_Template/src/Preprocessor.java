//=============================================================================
//CS5234 -- Distributed WordCount MapReduce Program -- The Pre-processor
//=============================================================================

public class Preprocessor {


	// This function takes and input String and outputs a processed
	// String according to the desired text-processing
	public static String PreProcess(String input)
	{
		String strClean = input;
		
		// Convert strClean to lower case to make it case insensitive
		// strClean = ...
		
		// Change words like can�t into cant
		strClean = strClean.replaceAll("�", ""); 
		
		// Replace all non-letter characters with a single space.
		// The non-letter characters will match the pattern: "[^a-zA-Z]"
		// strClean = ...
		
		// Trim the leading and trailing white spaces
		// strClean = ...
		
		return strClean;
	}

}
