import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

//=============================================================================
// CS5234 --  WordCount MapReduce Program
//=============================================================================

// The Driver class is where Hadoop parameters can be set such as the
// Split size, mapper and reducer input/output Key, Value types and
// input/output paths.
//
// It is also the place where command-line arguments can be processed
// and Usage options can be printed to the terminal. 

public class WordCountDriver extends Configured implements Tool {

//	static { 
//		Configuration.addDefaultResource("hdfs-default.xml"); 
//		Configuration.addDefaultResource("hdfs-site.xml"); 
//		Configuration.addDefaultResource("yarn-default.xml"); 
//		Configuration.addDefaultResource("yarn-site.xml"); 
//		Configuration.addDefaultResource("mapred-default.xml"); 
//		Configuration.addDefaultResource("mapred-site.xml");
//	}


	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.printf("Usage: %s <input-dir> <output-dir>\n\n",
					getClass().getSimpleName());
			ToolRunner.printGenericCommandUsage(System.err);
			return -1;
		}


		//Job job = Job.getInstance(new Configuration()); // this is a mistake; should pick up config which is already
		// set by GenericOptionParser and ToolRunner
		// otherwise all command line options, such as -libjars get igonred
		
		// Pick up configuration that has been set by 
		// GenericOptionParser and ToolRunner for further
		// customisation if necessary
		
		Configuration config = getConf();
		
		// Here we use config to alter the number of reduce tasks.
		// This will be important when running the job on a cluster.
//		String nredString = config.get("mapreduce.job.reduces");
//		int nred = Integer.parseInt(nredString);
//		if (nred < 4)
//			config.setInt("mapreduce.job.reduces", 4);
//		
		// Get an instance of the Job class
		// and use it to set various parameters for the MapReduce job
		// we want to run.
		Job job = Job.getInstance(config);
		job.setJarByClass(getClass());

		// Input and output paths are set from the
		// command line arguments
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));


		// Set Mapper, Combiner and Reducer classes
		job.setMapperClass(WordCountMapper.class);
		job.setCombinerClass(WordCountReducer.class);
		job.setReducerClass(WordCountReducer.class);

		// Set output key and value types for the mapper,
		// that is, the intermediate key/value types
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);

		// Output key and value types for the reducers
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		// Start the job and wait until it is completed.
		// Once completed, return its exit code (0 or 1).
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("Let's do some MapReduce!");
		// You have to always use ToolRunner to execute your driver program
		// to ensure configuration parameters 
		// and the command line options are properly picked up and processed. 
		int exitCode = ToolRunner.run(new WordCountDriver(), args);
		System.exit(exitCode);
	}

}
