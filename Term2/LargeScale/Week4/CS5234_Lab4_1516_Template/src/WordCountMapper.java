//=============================================================================
//CS5234 -- Distributed WordCount MapReduce Program -- The Mapper
//=============================================================================

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

public class WordCountMapper extends Mapper<Object, Text, Text, IntWritable> {

	private Text word = new Text();
	private IntWritable one = new IntWritable(1);

	// You can put instance variables here to store state between iterations of
	// the map task.

	// The setup method. Anything in here will be run exactly once before the
	// beginning of the map task.
	public void setup(Context context) throws IOException, InterruptedException {
//		Properties props = new Properties();
//		props.put("log4j.rootLogger", "DEBUG,default.file");
//		props.put("log4j.appender.default.file", "org.apache.log4j.FileAppender");
//		props.put("log4j.appender.default.file.append", true);
//		props.put("log4j.appender.default.file.file", "/home/local/uxac007/mylogfile.log");
//		props.put("log4j.appender.default.file.layout", "org.apache.log4j.PatternLayout");
//		props.put("log4j.appender.default.file.layout.ConversionPattern", "%-5p %c: %m%n");
//		PropertyConfigurator.configure(props);
	}

	// The map method
	public void map(Object key, Text value, Context context
			) throws IOException, InterruptedException {		

		// The FileInputFormat will supply input elements to the mapper
		// line-by-line using the line number as a key.
		
		// Get a line of input from the value and extract the status
		// using TwitterObjectFactory.createStatus
		
		String status = null;
		//try {
			// Get a line of input from the value and extract the status
			// using TwitterObjectFactory.createStatus. Do not forget
			// to catch and ignore TwitterException
		//} catch (TwitterException e) {
			//return;
		//}
		
		// This is the data cleansing step:
		// Put the status through the pre-processor
		// to get rid of unwanted characters
		String strLine = Preprocessor.PreProcess(status);			

		// Use StringTokenizer class to iterate over strLine
		// and extract words one by one as tokens.
		// For each token being extracted 
		// use the 'word' variable to set the intermediate
		// value and emit a pair (word, one) using
		// context.write(word, one)
		

	}

	// The cleanup method. Anything in here will be run exactly once after the
	// end of the map task.
	public void cleanup(Context context) throws IOException,
	InterruptedException {
		// Note: you can use Context methods to emit records in the setup and cleanup as well.

	}

}
