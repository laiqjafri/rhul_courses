import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.TimeZone;

import twitter4j.GeoLocation;
import twitter4j.HashtagEntity;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;
import twitter4j.User;


public class JsonETL {

	public static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSX";
	private static long groupCount = 0;

	/**
	 * @param args
	 * @throws IOException 
	 * @throws TwitterException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		//		System.out.println(escapeString("a'b"));
		//		System.out.println(escapeString("'a'b'dddddd'dddd'"));

		//		Scanner s = new Scanner(new File("/Users/uxac007/Work/BigData/CS5234/twotweets.json"));

		System.out.println("DELETE FROM fact_tweets *;");
		System.out.println("DELETE FROM dim_twitter_users *;");
		System.out.println("DELETE FROM dim_hashtag_groups *;");

		Scanner s = new Scanner(new File("/Users/uxac007/Work/BigData/CS5234/tweets100k.json"));

		while(s.hasNextLine()) {
			String jsonLine = s.nextLine();

			Status status;
			try {
				status = TwitterObjectFactory.createStatus(jsonLine);
			} catch (TwitterException e) {
				continue;
			}
			System.out.println("BEGIN;");
			emitInsertUserIntoDimTwitterUsers(status.getUser());
			emitInsertHashtagsIntoDimHashtagsGroup(status.getHashtagEntities());
			emitInsertStatusIntoTweetFacts(status);
			System.out.println("COMMIT;");
		}
		s.close();
	}

	private static String procString(String s) {
		StringBuffer sb = new StringBuffer();

		if (s != null) {
			sb.append("'");
			int j = 0;
			for (int i = s.indexOf("'", 0); i >= 0; i = s.indexOf("'", i + 1)) {
				sb.append(s.substring(j, i));
				sb.append("'");
				j = i;
			}
			sb.append(s.substring(j));
			sb.append("'");
			return sb.toString();
		}
		else 
			return "NULL";
	}

	private static String procInt(int val) {
		return val == -1 ? "NULL" : Integer.toString(val);
	}

	private static String procLong(long val) {
		return val == -1 ? "NULL" : Long.toString(val);
	}


	private static String getCoordinates(Status status) {
		GeoLocation geo = status.getGeoLocation();
		return geo != null ? "'( " + geo.getLatitude() + ", " + geo.getLongitude() + " )'" : "NULL";
	}

	/**
	 * @param status
	 * 
		tweet_dt                 | timestamp with time zone | 
 		from_dim_twitter_user_id | bigint                   | 
 		num_hashtags             | smallint                 | 
 		tweet_id                 | bigint                   | not null
 		tweet_status             | character varying(255)   | default NULL::character varying
 		coordinates              | point                    | 
 		dim_hashtag_group_id     | bigint                   | 

	 */
	private static void emitInsertStatusIntoTweetFacts(Status status) {
		HashtagEntity[] tags = status.getHashtagEntities();
		final SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO fact_tweets (tweet_dt, num_hashtags, tweet_id, tweet_status," +
				"from_dim_twitter_user_id, dim_hashtag_group_id, coordinates) VALUES (")
				.append("'" + sdf.format(status.getCreatedAt()) + "'")
				.append(", ").append(tags.length)
				.append(", ").append(procLong(status.getId()))
				.append(", ").append(procString(status.getText()))
				.append(", ").append(procLong(status.getUser().getId()))
				.append(", ").append(tags.length > 0 ? groupCount : "NULL")
				.append(", ").append(getCoordinates(status))
				.append(") ")
				.append("ON CONFLICT (tweet_id) DO NOTHING;");
		System.out.println(sb.toString());
	}

	/**
	 * @param user
	 *  user_id         | bigint                 | not null
 		name            | character varying(100) | default NULL::character varying
 		screen_name     | character varying(20)  | default NULL::character varying
 		location        | character varying(127) | default NULL::character varying
 		utc_offset      | integer                | default 0
 		time_zone       | character varying(127) | default NULL::character varying
 		followers_count | integer                | default 0
 		friends_count   | integer                | default 0
 		geo_enabled     | boolean                | default false
 		lang            | character varying(10)  | default NULL::character varying
	 */
	private static void emitInsertUserIntoDimTwitterUsers(User user) {
		// Add your code to populate dim_twitter_users table
		// according to the schema above using
		// the code in emitInsertStatusIntoTweetFacts as an example
	}


	/**
	 * @param tags
	 * @return
	 * 
	 *  group_id | bigint                 | not null default 0
 		hashtag  | character varying(140) | 
	 * 
	 */
	private static void emitInsertHashtagsIntoDimHashtagsGroup(HashtagEntity[] tags) {
		StringBuffer sb = new StringBuffer();

		if (tags.length > 0) {
			groupCount++;
			for (HashtagEntity tag : tags) {
				sb.append("INSERT INTO dim_hashtag_groups (group_id, hashtag) VALUES (")
				.append(groupCount)
				.append(", ").append(procString(tag.getText()))
				.append(");");
				System.out.println(sb.toString());
				sb.setLength(0);
			}
		}
	}
}

